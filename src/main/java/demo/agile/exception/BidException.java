package demo.agile.exception;

public class BidException extends RuntimeException{

    public BidException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public BidException() {
        super("Bid Exception");
    }
}
