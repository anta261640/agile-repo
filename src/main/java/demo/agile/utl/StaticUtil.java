package demo.agile.utl;

import org.springframework.util.Base64Utils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

public class StaticUtil {

    static public String decodeIt(String codeString) {
        if (!Optional.ofNullable(codeString).isPresent()) {
            return null;
        }
        byte[] bytes = Base64Utils.decodeFromString(codeString);
        return Base64.getEncoder().encodeToString(bytes);
    }

    static  public String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return format.format(date);
    }

}
