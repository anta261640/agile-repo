package demo.agile.task;

public interface Task {

    void init() throws Exception;

    void destroy() throws Exception;


}
