package demo.agile.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.agile.runner.RunnerBid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;

@Slf4j
@Component
@EnableScheduling
public class BidTask implements Task{

    @Autowired
    private RunnerBid runnerBid;

    @Override
    @Scheduled(fixedDelay =  60000)
    //Event-based reading from the stream
    //Read the objects from the attached file, consider the file contents is updated 1/min.
    public void init() throws Exception {
        log.info("Task for bid");
        ObjectMapper mapper = new ObjectMapper();
        List<LinkedHashMap> roots = mapper.readValue(new URL("http://localhost:8080/json"), List.class);
        runnerBid.setRootList(roots);
        runnerBid.start();
    }

    @Override
    public void destroy() throws Exception {
        runnerBid.stop();
    }
}
