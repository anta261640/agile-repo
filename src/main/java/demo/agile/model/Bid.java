package demo.agile.model;

import demo.agile.utl.StaticUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@Setter
@Getter
@ToString
public class Bid  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long ts;
    private TypeEnum ty;
    private String  pl;


    @Override
    public String toString(){
       return  "ID: " + id
               + " - TS: "  + StaticUtil.convertTime(ts)
               + " -  Type: " + ty
               + " - PL: " + StaticUtil.decodeIt(pl);
    }


}
