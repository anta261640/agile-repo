package demo.agile.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class Root  implements Serializable {
    private static final long serialVersionUID = 1L;

    private Bid bid;
}
