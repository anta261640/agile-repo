package demo.agile.runner;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.agile.model.Root;
import demo.agile.publisher.PublisherBid;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


@Slf4j
@Component
public class RunnerBid {


    private ThreadFactory tf;


    List<LinkedHashMap> rootList;
    private ExecutorService executor;

    @Autowired
    private PublisherBid publisher;

    @PostConstruct
    public void init() {
         tf = new BasicThreadFactory.Builder()
                .namingPattern("bid-replay-publisher:")
                .build();

    }

    public void start() {
        //Spawn a thread for each queue message
        executor = Executors.newSingleThreadExecutor(tf);
        executor.submit(this::pollAndPublish);
    }

    public void stop() {
        executor.shutdown();
    }

    public void pollAndPublish() {
        while (!executor.isShutdown()) {
            for (LinkedHashMap root : rootList) {
                publisher.publishMessage(new ObjectMapper().convertValue(root, Root.class));
            }
            stop();
        }

    }

    public void setRootList(List<LinkedHashMap> rootList) {
        this.rootList = rootList;
    }


}
