package demo.agile.publisher;


import demo.agile.exception.BidException;
import demo.agile.model.Bid;
import demo.agile.model.Root;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

import org.springframework.stereotype.Component;

import org.springframework.web.bind.annotation.*;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;


@RestController
@Component
@Slf4j
public class PublisherBid {

    @Autowired
    private JmsTemplate jmsTemplate;


    @RequestMapping(path = "/json", method = RequestMethod.GET)
    public ResponseEntity<Resource> jsonInHttp() throws IOException {
        InputStreamResource resource = new InputStreamResource(new FileInputStream("src/main/resources/bid.json"));

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(resource);
    }

    /**For test with postman*/
    @PostMapping("/publishMessage")
    public ResponseEntity<String> publishMessage(@RequestBody Root root) {
        isPresent(root);
        try {
            jmsTemplate.convertAndSend("payload-queue", root);
            return new ResponseEntity<>("Sent Bid.", HttpStatus.OK);

        } catch (BidException | JmsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    private boolean isPresent(Root root) {

        boolean is = Optional.ofNullable(root)
                .map(Root::getBid)
                .isPresent();
        if (is) {
            Bid b = root.getBid();
           // Use logging library to log output to console
            log.info("Bid present");
            log.info(b.toString());
            return true;
        } else {
            throw new BidException();
        }


    }


}
