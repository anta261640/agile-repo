package demo.agile.consumer;

import demo.agile.model.Root;
import demo.agile.publisher.PublisherBid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class ConsumerBid {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerBid.class);

    @Autowired
    private PublisherBid publishController;


    @JmsListener(destination = "payload-queue")
    public void messageListener(Root root) {
        LOGGER.info("Message received! {}", root.getBid());
    }





}
